package com.wirecard.account.sms;

import com.wirecard.account.transfer.TransferManager;
import com.wirecard.account.transfer.TransferManagerImpl;
import com.wirecard.account.user.UserManager;
import com.wirecard.account.user.UserManagerImpl;
import com.wirecard.account.utils.CommandHandler;
import com.wirecard.account.utils.InsufficientFundsException;
import com.wirecard.account.utils.UnknownCommandException;
import com.wirecard.account.utils.UnknownUserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class SMSHandlerImpl implements SMSHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    TransferManager transferManager;
    UserManager userManager;

    public static final String RESPONSE_OK = "OK";
    public static final String RESPONSE_ERR_INSUFFICIENT_FUNDS = "ERR – INSUFFICIENT FUNDS";
    public static final String RESPONSE_ERR_NO_USER = "ERR – NO USER";
    public static final String RESPONSE_ERR_UNKNOWN_COMMAND = "ERR – UNKNOWN COMMAND";
    public static final String RESPONSE_ERR = "ERROR";

    public SMSHandlerImpl() {
        transferManager = new TransferManagerImpl();
        userManager = new UserManagerImpl();
    }

    /**
     * @param smsContent     the incoming SMS command string.
     * @param senderDeviceId is a unique string that uniquely identifies the customer’s mobile device. The com.wirecard.account.user.UserManager proves a means to identify the sender user.
     * @return The SMS content that should be returned to the user.
     */
    public String handleSmsRequest(String smsContent, String senderDeviceId) {

        logger.debug("Got sms content: " + smsContent);

        String response = "";

        try {
            //authenticate user by device id
            String senderUsername = userManager.getUserNameForDeviceId(senderDeviceId);
            boolean isValidUser = isValidUser(senderUsername, userManager);
            if (!isValidUser) {
                //sender user not autheticated by device id
                logger.debug("Unknown user for sender device id: " + senderDeviceId);
                return RESPONSE_ERR_NO_USER;
            }

            //parse input sms content
            CommandHandler.CommandParseResponse commandParseResponse = CommandHandler.parse(smsContent);

            //handle use cases
            CommandHandler.Command command = commandParseResponse.getCommand();

            switch (command) {

                case BALANCE:
                    response = userManager.getBalance(senderUsername).toPlainString();
                    break;

                case SEND:
                    try {
                        //input arguments
                        String recipient = commandParseResponse.getTransferCommand().getUsername();
                        BigDecimal amount = commandParseResponse.getTransferCommand().getAmmount();

                        response = handleTransfer(senderUsername, recipient, amount, userManager, transferManager);

                    } catch (UnknownUserException e) {
                        logger.debug(e.getMessage());
                        return RESPONSE_ERR_NO_USER;
                    } catch (InsufficientFundsException e) {
                        logger.debug(e.getMessage());
                        return RESPONSE_ERR_INSUFFICIENT_FUNDS;
                    }
                    break;

                case TOTAL_SENT:

                    List<String> users = commandParseResponse.getUsersWithTotalSent();
                    List<String> totalSentToUsers = new LinkedList<>();
                    if (users != null && !users.isEmpty()) {

                        //check all user's validity in the list. if at least one is unknown , the whole command
                        //all checks clear, get all transactions to given users so far
                        for (String user : users) {

                            //check user validity
                            if (!isValidUser(user, userManager)) {
                                throw new UnknownUserException("Unknown user in total sent request user list: " + user);
                            }

                            List<BigDecimal> userTransactionsList = transferManager.getAllTransactions(senderUsername, user);
                            BigDecimal totalTransactionsPerUser = userTransactionsList.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
                            totalSentToUsers.add(totalTransactionsPerUser.toPlainString());
                        }

                        //return list of total transactions per user
                        response = String.join(",", totalSentToUsers);
                        logger.debug("Total transactions for users " + String.join(",", users) + ": " + response);

                    } else {
                        logger.debug("No user provided for total sent request");
                        response = RESPONSE_ERR_UNKNOWN_COMMAND;
                    }

                    break;

                default:
                    response = RESPONSE_ERR_UNKNOWN_COMMAND;
            }

        } catch (UnknownCommandException e) {
            logger.debug("Unknown command for content: " + smsContent);
            return RESPONSE_ERR_UNKNOWN_COMMAND;
        } catch (UnknownUserException e) {
            logger.debug("Unknown user for content: " + smsContent);
            return RESPONSE_ERR_NO_USER;
        } catch (Exception e) {
            logger.debug("Unexpected exception for content: " + smsContent, e);
            return RESPONSE_ERR;
        }

        //return response
        return response;
    }

    private String handleTransfer(String sender, String recipient, BigDecimal amount, UserManager userManager,
                                  TransferManager transferManager) throws UnknownUserException, InsufficientFundsException {

        //check for valid recipient user
        if (!userManager.existsUser(recipient)) throw new UnknownUserException("Unknown user " + recipient);

        //check sufficient funds on sender user side
        BigDecimal senderBalance = userManager.getBalance(sender);
        if (senderBalance.compareTo(amount) < 0) throw new InsufficientFundsException(
                "Insufficient funds for sender " + sender + ". Request to transfer " + amount +
                        " with current balance " + senderBalance); // ">="

        //if all checks are clear, transfer the money
        transferManager.sendMoney(sender, recipient, amount);

        return RESPONSE_OK;
    }

    private boolean isValidUser(String username, UserManager userManager) {
        return userManager.existsUser(username);
    }

}