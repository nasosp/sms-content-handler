package com.wirecard.account.transfer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

public class TransferManagerImpl implements TransferManager {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void sendMoney(String senderUsername, String recipientUsername, BigDecimal amount) {
        getLogger().debug("Successful transfer: " + amount.toPlainString() + " from " + senderUsername +
                " to " + recipientUsername);
    }

    @Override
    public List<BigDecimal> getAllTransactions(String senderUsername, String recipientUsername) {
        return null;
    }

    private Logger getLogger() {
        if(logger == null) logger = LoggerFactory.getLogger(this.getClass());
        return logger;
    }
}
