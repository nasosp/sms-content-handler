package com.wirecard.account.user;

import java.math.BigDecimal;

public class UserManagerImpl implements UserManager {

    public static final String USER_FFRITZ = "FFRITZ";
    public static final String USER_NASOSP = "NASOSP";
    public static final String USER_MSMITH = "MSMITH";

    @Override
    public boolean existsUser(String username) {
        return false;
    }

    @Override
    public BigDecimal getBalance(String username) {
        return null;
    }

    @Override
    public String getUserNameForDeviceId(String deviceId) {
        return null;
    }
}
