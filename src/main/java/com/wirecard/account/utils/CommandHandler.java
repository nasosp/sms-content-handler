package com.wirecard.account.utils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class CommandHandler {

    public enum Command {
        BALANCE("BALANCE"),
        SEND("SEND"),
        TOTAL_SENT("TOTAL-SENT");

        private String commandLabel;

        Command(String commandLabel) {
            this.commandLabel = commandLabel;
        }

        public String getCommandLabel() {
            return commandLabel;
        }

        public static Command ofStrValue(String s) {
            return Arrays.stream(values()).filter(v -> s.compareTo(v.getCommandLabel()) == 0).findFirst().orElse(null);
        }
    }

    /**
     * @param content
     * @return
     * @throws UnknownCommandException
     */
    public static CommandParseResponse parse(String content) throws UnknownCommandException {

        CommandParseResponse commandParseResponse = null;

        //split content to basic elements
        if (content == null || content.isEmpty()) return null; //safe-guard
        String[] input = content.split("-");

        if (input.length >= 1) {

            Command command = null;
            List<String> users = null;
            TransferCommand transferCommand = null;

            //actual parsing with rules
            //case of: length == 1 and command input valid
            if (input.length == 1 && !input[0].isEmpty()) {

                command = getCommand(input[0]);

            } else if (input.length > 1) {

                //attempt to recognize single element command
                command = Command.ofStrValue(input[0]);
                if (command != null && input.length == 3) { //command recognized, parse arguments

                    transferCommand = new TransferCommand(input[2], new BigDecimal(input[1]));

                } else if (command == null) {  //command not recognized, attempt to parse as double element

                    //if again command not recognized, throw an UnknownCommandException
                    command = getCommand(String.join("-", input[0], input[1]));
                    if (command != null) {//command recognized as double element

                        //make a list of arguments, iterate the rest of the input array, apart from the two first command slots
                        String[] args = Arrays.copyOfRange(input, 2, input.length);
                        if (args.length > 0) {
                            users = Arrays.asList(args);
                        }

                    } else {
                        //command eventually recognized. throw unknown command exception
                        throw new UnknownCommandException("Unknown command " + content);
                    }

                }

            }

            //response object from parsed command
            commandParseResponse = new CommandParseResponse(command, users, transferCommand);
        }

        return commandParseResponse;
    }

    /**
     * @param s
     * @return
     * @throws UnknownCommandException
     */
    private static Command getCommand(String s) throws UnknownCommandException {
        Command c = Command.ofStrValue(s);
        if (s == null || c == null) throw new UnknownCommandException("Unknown command: " + s);
        return c;
    }

    public static class TransferCommand {
        private String username;
        private BigDecimal ammount;

        public TransferCommand(String username, BigDecimal ammount) {
            this.username = username;
            this.ammount = ammount;
        }

        public String getUsername() {
            return username;
        }

        public BigDecimal getAmmount() {
            return ammount;
        }
    }


    public static class CommandParseResponse {
        private Command command;
        private List<String> usersWithTotalSent;
        private TransferCommand transferCommand;

        public CommandParseResponse(Command command, List<String> usersWithTotalSent, TransferCommand transferCommand) {
            this.command = command;
            this.usersWithTotalSent = usersWithTotalSent;
            this.transferCommand = transferCommand;
        }

        public Command getCommand() {
            return command;
        }

        public List<String> getUsersWithTotalSent() {
            return usersWithTotalSent;
        }

        public TransferCommand getTransferCommand() {
            return transferCommand;
        }
    }
}
