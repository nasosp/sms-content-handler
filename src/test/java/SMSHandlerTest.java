import com.wirecard.account.sms.SMSHandler;
import com.wirecard.account.sms.SMSHandlerImpl;
import com.wirecard.account.transfer.TransferManagerImpl;
import com.wirecard.account.user.UserManager;
import com.wirecard.account.user.UserManagerImpl;
import com.wirecard.account.utils.CommandHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Collections;

import static com.wirecard.account.sms.SMSHandlerImpl.RESPONSE_ERR_NO_USER;
import static com.wirecard.account.user.UserManagerImpl.USER_FFRITZ;
import static com.wirecard.account.user.UserManagerImpl.USER_NASOSP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SMSHandlerTest {

    private static Logger logger = LoggerFactory.getLogger(SMSHandlerTest.class);

    @Mock
    private UserManager userManager;

    @Mock
    private TransferManagerImpl transferManager;

    @Spy
    @InjectMocks
    private SMSHandler smsHandler = new SMSHandlerImpl();

    private static final String BALANCE = CommandHandler.Command.BALANCE.getCommandLabel();
    private static final String SEND_100_FFRITZ = CommandHandler.Command.SEND.getCommandLabel() + "-100-" + UserManagerImpl.USER_FFRITZ;
    private static final String TOTAL_SEND_FFRITZ = CommandHandler.Command.TOTAL_SENT.getCommandLabel() + "-" + UserManagerImpl.USER_FFRITZ;
    private static final String TOTAL_SEND_FFRITZ_MSMITH = CommandHandler.Command.TOTAL_SENT.getCommandLabel() + "-" +
            UserManagerImpl.USER_FFRITZ + "-" + UserManagerImpl.USER_MSMITH;
    private static final String TOTAL_SEND_FFRITZ_MSMITH_UKNOWN = TOTAL_SEND_FFRITZ_MSMITH + "-" + "DUMMY";
    private static final String UNKNOWN_COMMAND = "XYZ";

    private static final String DEVICE_ID = "vu47hbsdbvbfsbfbfw";
    private static final String DEVICE_ID_FALSE = "gdgfg898dbvbfsbfbfw";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        logger.info("----------------- Test case start ---------------------");
    }

    @After
    public void tearDown() throws Exception {
        logger.info("----------------- Test case complete ------------------\n");
    }

    @Test
    public void testHandleSmsRequest_Balance() {

        when(userManager.getBalance(USER_NASOSP)).thenReturn(new BigDecimal("1500.0"));
        when(userManager.getUserNameForDeviceId(DEVICE_ID)).thenReturn(UserManagerImpl.USER_NASOSP);
        when(userManager.existsUser(UserManagerImpl.USER_NASOSP)).thenReturn(true);

        String response = smsHandler.handleSmsRequest(BALANCE, DEVICE_ID);
        assertNotNull(response);
        assertEquals(response, "1500.0");

    }

    @Test
    public void testHandleSmsRequest_SendMoney() {

        //use cases: successful, no user, insufficient funds

        //1. Successful transfer
        when(userManager.existsUser(UserManagerImpl.USER_NASOSP)).thenReturn(true);
        when(userManager.existsUser(UserManagerImpl.USER_FFRITZ)).thenReturn(true);
        when(userManager.getBalance(USER_NASOSP)).thenReturn(new BigDecimal("101"));
        when(userManager.getUserNameForDeviceId(DEVICE_ID)).thenReturn(USER_NASOSP);
        doCallRealMethod().when(transferManager).sendMoney(USER_NASOSP, USER_FFRITZ, new BigDecimal("100"));

        String response = smsHandler.handleSmsRequest(SEND_100_FFRITZ, DEVICE_ID);
        assertNotNull(response);
        assertEquals(response, SMSHandlerImpl.RESPONSE_OK);

        //2.1 unknown recipient user
        response = smsHandler.handleSmsRequest(SEND_100_FFRITZ + "99", DEVICE_ID);
        assertNotNull(response);
        assertEquals(response, RESPONSE_ERR_NO_USER);

        //2.2 unknown sender user
        response = smsHandler.handleSmsRequest(SEND_100_FFRITZ, DEVICE_ID_FALSE);
        assertNotNull(response);
        assertEquals(response, RESPONSE_ERR_NO_USER);

        //3. insufficient funds
        when(userManager.getBalance(USER_NASOSP)).thenReturn(new BigDecimal("99"));

        response = smsHandler.handleSmsRequest(SEND_100_FFRITZ, DEVICE_ID);
        assertNotNull(response);
        assertEquals(response, SMSHandlerImpl.RESPONSE_ERR_INSUFFICIENT_FUNDS);
    }

    @Test
    public void testHandleSmsRequest_GetTotalSent() {

        //use cases: successful for single user, successful for 2 users, no user

        //1. Successful total sent retrieval for single user
        when(userManager.existsUser(UserManagerImpl.USER_NASOSP)).thenReturn(true);
        when(userManager.existsUser(UserManagerImpl.USER_FFRITZ)).thenReturn(true);
        when(userManager.existsUser(UserManagerImpl.USER_MSMITH)).thenReturn(true);
        when(userManager.getUserNameForDeviceId(DEVICE_ID)).thenReturn(USER_NASOSP);
        when(transferManager.getAllTransactions(UserManagerImpl.USER_NASOSP, UserManagerImpl.USER_FFRITZ))
                .thenReturn(Collections.singletonList(new BigDecimal("560")));

        String response = smsHandler.handleSmsRequest(TOTAL_SEND_FFRITZ, DEVICE_ID);
        assertNotNull(response);
        assertEquals(response, "560");

        //2. Successful total sent retrieval for multiple users
        when(transferManager.getAllTransactions(UserManagerImpl.USER_NASOSP, UserManagerImpl.USER_MSMITH))
                .thenReturn(Collections.singletonList(new BigDecimal("250")));

        response = smsHandler.handleSmsRequest(TOTAL_SEND_FFRITZ_MSMITH, DEVICE_ID);
        assertNotNull(response);
        assertEquals(response, "560,250");

        //3. Failed total sent retrieval for multiple users (one of the users is unknown)
        response = smsHandler.handleSmsRequest(TOTAL_SEND_FFRITZ_MSMITH_UKNOWN, DEVICE_ID);
        assertNotNull(response);
        assertEquals(response, RESPONSE_ERR_NO_USER);

    }

    @Test
    public void testHandleSmsRequest_UnknownCommand() {

        //unknown command
        when(userManager.existsUser(UserManagerImpl.USER_NASOSP)).thenReturn(true);
        when(userManager.getUserNameForDeviceId(DEVICE_ID)).thenReturn(USER_NASOSP);

        String response = smsHandler.handleSmsRequest(UNKNOWN_COMMAND, DEVICE_ID);
        assertNotNull(response);
        assertEquals(response, SMSHandlerImpl.RESPONSE_ERR_UNKNOWN_COMMAND);

    }
}